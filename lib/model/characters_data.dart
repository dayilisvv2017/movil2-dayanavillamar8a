import 'character.dart';

final characters = [
  Character(
    name: 'Martín Cedeño',
    age: 28,
    image: 'images/martin.png',
    jobTitle: 'Flutter Developer',
    stars: 4.3,
  ),
  Character(
    name: 'Didier Lopez',
    age: 21,
    image: 'images/didier.png',
    jobTitle: 'Android Developer',
    stars: 3.8,
  ),
  Character(
    name: 'Grace Castro',
    age: 33,
    image: 'images/Grace.png',
    jobTitle: 'iOS Developer',
    stars: 4.1,
  ),
  Character(
    name: 'Mateo Martin',
    age: 25,
    image: 'images/mateo.png',
    jobTitle: 'Desarrollador de aplicaciones web',
    stars: 3.1,
  ),
  Character(
    name: 'Juleydi Moragues',
    age: 23,
    image: 'images/jule.png',
    jobTitle: 'Programador web',
    stars: 3.7,
  ),
  Character(
    name: 'Grabriel Cedeño',
    age: 26,
    image: 'images/gabriel.png',
    jobTitle: ' Gestor de seguridad',
    stars: 3.3,
  ),
  Character(
    name: 'David Quispe',
    age: 30,
    image: 'images/david.png',
    jobTitle: 'Estratega digital',
    stars: 4.1,
  ),
  Character(
    name: 'Rocio Hernadez',
    age: 29,
    image: 'images/rocio.png',
    jobTitle: 'Project Manager',
    stars: 3.8,
  ),
  Character(
    name: 'Ricardo Arcentales',
    age: 26,
    image: 'images/Ricardo.png',
    jobTitle: 'Consultor para empresas',
    stars: 3.0,
  ),
  Character(
    name: 'Lucia Martin',
    age: 30,
    image: 'images/lucia.png',
    jobTitle: 'Programador industrial',
    stars: 4.2,
  ),
];
